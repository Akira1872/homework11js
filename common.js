const passwordForm = document.querySelector('.password-form');
const button = document.querySelector('.btn');

passwordForm.addEventListener('click', (e) => {
    if (e.target.classList.contains('icon-password')) {
        const container = e.target.closest('.input-wrapper');
        const input = container.querySelector('input');
        if (input.type === 'password') {
            input.setAttribute('type', 'text');
            e.target.style.display = 'none';
            const slashIcon = container.querySelector('.fa-eye-slash');
            slashIcon.style.display = 'block';
        } else {
            input.setAttribute('type', 'password');
            e.target.style.display = 'none';
            const eyeIcon = container.querySelector('.fa-eye');
            eyeIcon.style.display = 'block';
        }
    }
});

button.addEventListener('click', (e) => {
    e.preventDefault();
    const passwordInputEnter = document.getElementById('enter');
    const passwordInputConfirm = document.getElementById('confirm');

    const enterPassword = passwordInputEnter.value;
    const confirmPassword = passwordInputConfirm.value;

    if (enterPassword === confirmPassword) {
        alert('You are welcome');
        passwordInputEnter.value = '';
        passwordInputConfirm.value = '';
        const warning = document.querySelector('.warning');
        if (warning) {
            warning.remove();
        }
    } else {
        const wrapper = document.getElementById('wrapper');
        let warning = wrapper.querySelector('.warning');
    
        if (!warning) {
            warning = document.createElement('span');
            warning.textContent = 'You must enter the same values';
            warning.style.color = 'red';
            warning.classList.add('warning');
            wrapper.appendChild(warning);
        }
    }
});
